<div class="elos-mobile d-xl-none"><div class="accordion" id="accordionElos"> <?php
            $argsElosMobile = array(
                'post_type' => 'nossos-elos',
                'posts_per_page' => -1,
                
            );
            $elosMobile = new WP_Query($argsElosMobile);
            if($elosMobile->have_posts()): while($elosMobile->have_posts()) : $elosMobile->the_post();
        ?> <div class="card"><div class="card-header" id="heading-<?php echo $post->post_name; ?>" style="background-color:<?= get_field('cor_do_elo') ?>"><div class="botao-acordeao" data-toggle="collapse" data-target="#collapse-<?php echo $post->post_name; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $post->post_name; ?>"><div class="grupinho"> <?php the_post_thumbnail(); ?> <h2><?php the_title(); ?></h2></div></div></div><div id="collapse-<?php echo $post->post_name; ?>" class="collapse" aria-labelledby="heading-<?php echo $post->post_name; ?>" data-parent="#accordionElos"><div class="card-body"> <?php 
                        if($post->ID == 382):
                    ?> <a href="https://www.instagram.com/circulaplastico/" target="_blank"><img src="<?= the_field('imagem_consumidores');?>" alt=""></a> <?php else: ?> <div class="carossel-clientes-mobile-<?php echo $post->post_name; ?>"> <?php 
                            $galeria = get_field('logo_dos_clientes');
                            $contador_logos = 1;
                            $total_logos = count($galeria);
                            // var_dump($total_logos);
                            foreach($galeria as $foto):
                                // var_dump($foto);
                                if($contador_logos %4 == 1):
                            ?> <div class="item"><div class="row"> <?php endif; ?> <div class="col-6"><img src="<?php echo $foto['sizes']['medium'] ?>" alt=""></div> <?php if($contador_logos %4 == 0 || $contador_logos == $total_logos): ?> </div></div> <?php endif; ?> <?php $contador_logos++; endforeach; ?> </div> <?php endif; ?> </div></div></div> <?php endwhile; endif; wp_reset_postdata(); ?> </div></div><div class="elos-desktop d-none d-xl-block"><div class="container"><div class="row align-items-center"><div class="col-6"><div class="menu-tabs"><ul class="nav nav-tabs" id="myTab" role="tablist"> <?php
                            $argsElosDesktop = array(
                                'post_type' => 'nossos-elos',
                                'posts_per_page' => -1,
                                
                            );
                            $elosDesktop = new WP_Query($argsElosDesktop);
                        ?> <?php if($elosDesktop->have_posts()): while($elosDesktop->have_posts()) : $elosDesktop->the_post(); ?> <li class="nav-item" role="presentation"><a class="nav-link tab-elo <?php if($post->ID == 382): ?> active <?php endif; ?>" id="<?php echo $post->post_name ?>-tab" data-toggle="tab" data-bola="<?= get_field('cor_do_elo_desktop') ?>" href="#<?php echo $post->post_name ?>" role="tab" aria-controls="<?php echo $post->post_name ?>" aria-selected="true" style="background-color:<?= get_field('cor_do_elo_desktop') ?>"> <?php the_post_thumbnail(); ?> </a></li> <?php endwhile; endif; wp_reset_postdata(); ?> </ul><div class="rodar-menu"><!-- <img class="circulo-menu" id="circulo-menu-rodar" src="<?= get_stylesheet_directory_uri() ?>/dist/img/menu-rodar.png" alt="">     --><div class="circulo-menu" id="circulo-menu-rodar"><div class="bola" id="bola-menu"></div></div><img class="texto-menu-circular fade show" src="<?= get_stylesheet_directory_uri() ?>/dist/img/texto-menu.png" alt=""> <?php if($elosDesktop->have_posts()): while($elosDesktop->have_posts()) : $elosDesktop->the_post(); ?> <p class="texto-menu-circular <?php echo $post->post_name ?>-tab fade"><?php the_title() ?></p> <?php endwhile; endif; wp_reset_postdata(); ?> </div></div></div><div class="col-6"><div class="conteudo-tabs"><div class="tab-content" id="myTabContent"> <?php if($elosDesktop->have_posts()): while($elosDesktop->have_posts()) : $elosDesktop->the_post(); ?> <div class="tab-pane fade <?php if($post->ID == 382): ?> show active <?php endif; ?>" id="<?php echo $post->post_name ?>" role="tabpanel" aria-labelledby="<?php echo $post->post_name ?>-tab"> <?php if($post->ID == 382): ?> <a href="https://www.instagram.com/circulaplastico/" target="_blank"><img src="<?= the_field('imagem_consumidores');?>" alt=""></a> <?php else: ?> <div class="carossel-clientes-desktop-<?php echo $post->post_name; ?>"> <?php 
                                $galeria = get_field('logo_dos_clientes');
                                $contador_logos = 1;
                                $total_logos = count($galeria);
                                // var_dump($total_logos);
                                foreach($galeria as $foto):
                                    // var_dump($foto);
                                    if($contador_logos %8 == 1):
                                ?> <div class="item"><div class="row"> <?php endif; ?> <div class="col-6 pb-4"><img src="<?php echo $foto['url'] ?>" alt=""></div> <?php if($contador_logos %8 == 0 || $contador_logos == $total_logos): ?> </div></div> <?php endif; ?> <?php $contador_logos++; endforeach; ?> </div> <?php endif; ?> </div> <?php endwhile; endif;  wp_reset_postdata(); ?> </div></div></div></div></div></div>