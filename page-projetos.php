<?php get_header(); ?> <section class="projetos"><div class="intro container d-lg-flex justify-content-between"><div class="col-lg-5 px-0"> <?= get_template_part('breadcrumbs'); ?> <h2 class="title"><?= the_title(); ?></h2><div class="line-title"></div><p><?= the_content(); ?></p></div><img class="d-lg-block d-none col-lg-6 pr-0" src="<?= the_post_thumbnail_url(); ?>" alt=""></div><img class="d-lg-none" src="<?= the_post_thumbnail_url(); ?>" alt=""><div class="projetos-box container"> <?php
    $argsProjetos = array(
      'post_type' => 'projeto',
      'order' => 'ASC',


    );
    $projetos = new WP_Query($argsProjetos);


    if ($projetos->have_posts()) :; ?> <?php while ($projetos->have_posts()) :  $projetos->the_post(); ?> <div class="item d-md-flex col-lg-6 px-0"><div class="img col-lg-4 col-md-5 px-0" style="background: url(<?= get_the_post_thumbnail_url(); ?>);"></div><div class="wrapper"><h4 class="titulo"><?= the_title(); ?></h4><div class="line-title"></div><p><?= the_content(); ?></p> <?php if (get_field('arquivo')) : $tipo = ' arquivo';
            else : $tipo = ' link-modal';
            endif; ?> <a href="<?= the_field('url_link'); ?>" target="_blank" class="btn-cta <?= $tipo; ?>">Acessar <i class="ml-2 fas fa-arrow-right"></i></a></div></div> <?php endwhile;
    endif; ?> </div><div class="modal-form"><div class="modal-content"><div class="header"><h4 class="">Preencha para visualizar:</h4><a href="#" class="close-modal"><i class="far fa-times-circle"></i></a></div><div class="form"> <?= do_shortcode('[contact-form-7 id="236" title="Formulário PDF"]'); ?> <script>document.addEventListener('wpcf7mailsent', function(event) {

            var link = document.createElement('a');
            link.href = '<?= the_field('arquivo'); ?>';
            link.download = '<?= the_title(); ?>';
            link.dispatchEvent(new MouseEvent('click'));

          }, false);</script></div></div></div></section> <?php get_footer(); ?>