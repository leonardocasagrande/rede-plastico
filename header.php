<!DOCTYPE html><html lang="pt_BR"><head><meta name="adopt-website-id" content="3518b677-30c5-40de-b626-54d89645222a"><script src="//tag.goadopt.io/injector.js?website_code=3518b677-30c5-40de-b626-54d89645222a" class="adopt-injector"></script><!-- Google Tag Manager --><script>(function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-WPGRTD3');</script><!-- End Google Tag Manager --><script type="text/javascript">window.smartlook || (function(d) {
      var o = smartlook = function() {
          o.api.push(arguments)
        },
        h = d.getElementsByTagName('head')[0];
      var c = d.createElement('script');
      o.api = new Array();
      c.async = true;
      c.type = 'text/javascript';
      c.charset = 'utf-8';
      c.src = 'https://rec.smartlook.com/recorder.js';
      h.appendChild(c);
    })(document);
    smartlook('init', '34f8c0b1d97d747a50f4ec28de5bbfc4896d26f5');</script><meta charset="UTF-8"><meta name="viewport" content="width=device-width,initial-scale=1"><title> <?= wp_title(); ?></title><meta name="robots" content="index, follow"><meta name="msapplication-TileColor" content="#ffffff"><meta name="theme-color" content="#ffffff"> <?php wp_head(); ?> <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css" integrity="sha512-ZKX+BvQihRJPA8CROKBhDNvoc2aDMOdAlcm7TUQY+35XYtrd3yh95QOOhsPDQY9QnKE0Wqag9y38OIgEvb88cA==" crossorigin="anonymous" referrerpolicy="no-referrer"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css"><link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css"></head><body><!-- Google Tag Manager (noscript) --><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WPGRTD3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><!-- End Google Tag Manager (noscript) --><header class=""> <?php



    if (!is_page('home')) : $isnt_home = ' is-not-home';
      $custom_height = 'height-custom';
      $isnt_home_pre = ' is-not-home-pre';
    else : {
        $custom_height = ' ';
        $isnt_home = ' ';
        $isnt_home_pre = ' ';
        $is_home = ' is-home';
      }


    endif;     ?> <div class="pre <?= $isnt_home_pre; ?> <?= $is_home; ?>"><a class="col-7 col-lg-2 px-0 d-none d-lg-block custom-logo" href="<?= get_site_url(); ?>"><img class="" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="rede"></a><!-- <div class=" d-flex col-lg-9 align-items-center justify-content-between px-0"> --><div class="links d-lg-flex align-items-center d-none"><a href="<?= get_site_url(); ?>/a-rede">A REDE</a> <a href="<?= get_site_url(); ?>/nossos-elos">Nossos Elos </a><a href="<?= get_site_url(); ?>/frentes-de-trabalho">Frentes de Trabalho</a><div class="dropdown"><a class="" type="button" id="dropdownMenuButton" href="<?= get_site_url(); ?>/conteudos">Conteúdos </a><button type="button" class="p-0 me-4" data-toggle="dropdown" aria-expanded="false"><span class="visually-hidden">Toggle Dropdown</span></button><div class="dropdown-menu" aria-labelledby="dropdownMenuButton"><a class="dropdown-item" href="<?= get_site_url(); ?>/ebooks" target="_blank">E-books</a> <a class="dropdown-item" href="<?= get_site_url(); ?>/blog" target="_blank">Blog</a></div></div><!-- <a href="<?= get_site_url(); ?>/projetos">Projetos </a> --><!-- <a href="<?= get_site_url(); ?>/retorna">Retorna </a> --> <a href="<?= get_site_url(); ?>/eventos">Eventos </a><a href="<?= get_site_url(); ?>/contato">Contato</a> <a href="<?= get_site_url(); ?>/retorna-projeto"><img style="width: 100px;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/retorna-logo.png" alt=""></a><a class="btn-faca" href="<?= get_site_url(); ?>/faca-parte">Faça Parte</a><!-- <a class="d-none d-lg-block" href="http://redeplastico.com.br/site/rede-indice.asp?secao=8"><i class="fas fa-lock pr-2"></i> Área Restrita</a> --><!-- <a class="btn-restrict d-lg-none" href="http://redeplastico.com.br/site/rede-indice.asp?secao=8"><i class="fas fa-lock pr-2"></i> Área Restrita</a> --></div><!-- <div class="btn-midias d-none d-lg-flex col-lg-2 px-lg-0">

          <a href="https://www.instagram.com/circulaplastico/" target="_blank">
            <i class="fab fa-instagram"></i>
          </a>

          <a href="https://www.linkedin.com/company/circulaplastico/" target="_blank">
            <i class="fab fa-linkedin"></i>
          </a>

          <a href="https://www.facebook.com/circulaplastico" target="_blank">
            <i class="fab fa-facebook-f"></i>
          </a>

        </div> --><!-- </div> --><div class="linear-gradient"></div></div><div class="<?= $custom_height; ?>"></div><nav class="top-nav d-lg-none <?= $isnt_home; ?> <?= $is_home; ?> " id="top-nav"><a class="col-4 col-md-2 px-0 d-lg-none" href="<?= get_site_url(); ?>"><img class="logo-header" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="rede"></a><div class="menu-box"><input type="checkbox" class="menu-btn" id="toggle-text-on-left"> <label id="menu-icon" for="toggle-text-on-left"><i></i></label></div><div class="menu"><!-- <a href="<?= get_site_url(); ?>/o-plastico">O Plástico</a> --> <a href="<?= get_site_url(); ?>/a-rede">A REDE</a> <a href="<?= get_site_url(); ?>/nossos-elos">Nossos Elos </a><a href="<?= get_site_url(); ?>/frentes-de-trabalho">Frentes de Trabalho </a><a href="<?= get_site_url(); ?>/conteudos">Conteúdos </a><!-- <a href="<?= get_site_url(); ?>/projetos">Projetos </a> --><!-- <a href="<?= get_site_url(); ?>/retorna">Retorna </a> --> <a href="<?= get_site_url(); ?>/eventos">Eventos </a><a href="<?= get_site_url(); ?>/contato">Contato</a> <a href="<?= get_site_url(); ?>/retorna-projeto"><img style="width: 180px;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/retorna-logo.png" alt=""></a><a class="btn-faca" href="<?= get_site_url(); ?>/faca-parte">Faça Parte</a><!-- <a class="btn-restrict d-lg-none" href="http://redeplastico.com.br/site/rede-indice.asp?secao=8"><i class="fas fa-lock pr-2"></i> Área Restrita</a> --><div class="d-lg-flex pt-lg-5 col-lg-9 justify-content-between px-0"><!-- <a class="btn-restrict d-none d-lg-flex mt-lg-0" href="http://redeplastico.com.br/site/rede-indice.asp?secao=8"><i class="fas fa-lock pr-2"></i> Área Restrita</a> --><div class="midias-header px-0 col-4 mt-lg-0"><a href="" target="_blank"><i class="fab fa-instagram"></i></a> <a href="" target="_blank"><i class="fab fa-linkedin"></i></a> <a href="" target="_blank"><i class="fab fa-facebook-f"></i></a></div></div></div></nav></header></body></html>