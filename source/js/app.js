$("#menu-icon").on("click", function () {
  $(".top-nav").toggleClass("ativo");
});

$(".menu-box").on("click", function () {
  $(".top-nav").toggleClass("ativo");
  if ($(".top-nav").hasClass("ativo")) {
    $(".menu-btn")[0].checked = true;
  } else {
    $(".menu-btn")[0].checked = false;
  }
});

$(document).on("scroll", function () {
  if ($(document).scrollTop() >= 140) {
    $(".is-home").addClass("fixed");
  } else {
    $(".is-home").removeClass("fixed");
  }
});

$(".top-button").on("click", function (e) {
  e.preventDefault();
  window.scrollTo(0, 0);
});

$('.tab-elo[data-toggle="tab"]').on("shown.bs.tab", function (event) {
  event.target; // newly activated tab
  event.relatedTarget; // previous active tab
  var idAtivo = event.target.id;
  var idAnterior = event.relatedTarget.id;
  var imgRotativa = $("#circulo-menu-rodar");
  var container = $(".rodar-menu");
  var classeAtiva = "." + idAtivo;
  var bola = $("#bola-menu");
  var seletorBola = "#"+idAtivo;
  var corBola = $(seletorBola).attr("data-bola");
  // var classeAnterior = "."+idAnterior;
  container.find($(".show")).removeClass("show");
  container.find(classeAtiva).addClass("show");
  imgRotativa.removeClass(idAnterior);
  imgRotativa.addClass(idAtivo);
  bola.css("background-color", corBola);
  console.log($(seletorBola).attr("data-bola"));
});

// PDF PROJETOS

$(".arquivo").on("click", function (e) {
  e.preventDefault();
  $(".modal-form").attr("style", "display:flex");
});

$(".close-modal").on("click", function (e) {
  e.preventDefault();
  $(".modal-form").attr("style", "display:none");
});

// Validação de campo

function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

$(".e-mail").attr("onChange", "validateEmail(this)");

// $(".phone").inputmask({
//   placeholder: "",
//   mask: function () {
//     return ["(99) 9999-9999", "(99) 99999-9999"];
//   },
// });

$(".custom-cnpj").inputmask({
  placeholder: "",
  mask: function () {
    return "99.999.999/9999-99";
  },
});

$(".btn a ").on("click", function () {
  $(".btn a i").removeClass("fa-times");
  $(".btn a i").addClass("fa-plus");
  this.children[0].classList.add("fa-times");
});
