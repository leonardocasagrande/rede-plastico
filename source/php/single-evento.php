<?php the_post();
$paged = (isset($_GET['pagina']) ? $_GET['pagina'] : 1); ?>

<?= get_header(); ?>



<section class="single-eventos">
  <div class="d-lg-flex wrapper-lg justify-content-between">
    <div class="container px-lg-0 mx-lg-0 col-lg-5">
      <?= get_template_part('breadcrumbs'); ?>

      <h3 class="title"><?= the_title(); ?></h3>
      <div class="line-title"></div>

      <p><?= the_content(); ?></p>

    </div>

    <div class="gray-box col-lg-6">
      <div class="container">

        <div class="data-local">
          <h4 class="sub">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/data.png" alt="data-icon ">
            Data
          </h4>

          <span class="data"><?= the_field('data'); ?></span>
        </div>

        <div class="data-local">
          <h4 class="sub">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/local.png" alt="data-icon ">
            Local
          </h4>

          <span class="data"><?= the_field('local'); ?></span>
        </div>

      </div>
    </div>
  </div>



  <div class="carousel-fotos d-lg-none">

    <?php
    $images = get_field('fotos_eventos');


    $size = 'full'; // (thumbnail, medium, large, full or custom size)
    if ($images) : ?>
      <?php foreach ($images as $image_id) : ?>
        <div class="item" style="background: url(<?= $image_id; ?>);"></div>
      <?php endforeach; ?>
    <?php endif; ?>

  </div>



  <div class="galeria-lg d-none d-lg-block  container ">

    <?php

    $images =  get_field('fotos_eventos');
    $size = 'full'; // (thumbnail, medium, large, full or custom size)

    ?>

    <div class="maior col-12 px-0" style="background: url(<?= $images[0]; ?>);"></div>

    <?php

    if ($images) : ?>
      <div class="wrapper">
        <div class="carousel-galeria ">

          <?php foreach ($images as $image_id) : ?>
            <a href="<?= $image_id; ?>" data-lightbox="roadtrip" data-title="">
              <img class="menor" src="<?= $image_id; ?>">
            </a>
          <?php endforeach; ?>

        </div>
      </div>

    <?php endif; ?>



  </div>




  <div class="container eventos-estrutura anteriores pt-5">

    <h4 class="sub">Eventos anteriores</h4>
    <div class="wrapper">

      <?php
      wp_reset_postdata();
      $argsEventos =   array(
        'post_type' => 'evento',
        'order' => 'ASC',
        'posts_per_page' => '2',
        'paged' => $paged,


        'tax_query' => array(
          array(
            'taxonomy' => 'situacao',
            'field' => 'slug',
            'terms' => 'eventos-realizados',
          )

        )
      );

      $eventosRealizados = new WP_Query($argsEventos);


      while ($eventosRealizados->have_posts()) : $eventosRealizados->the_post();


      ?>


        <div class="item d-lg-flex col-lg-6">
          <div class="bg-item" style="background: url(<?= the_post_thumbnail_url(); ?>); background-position: center;"></div>
          <div class="infos col-lg-7">

            <h5 class="titulo"><?= the_title(); ?></h5>
            <div class="line-title"></div>

            <!-- <p><?= the_content(); ?></p> -->

            <div class="data-local pt-3">
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/data.png" alt="data-icon">
              <span><?= the_field('data'); ?></span>
            </div>

            <div class="data-local">
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/local.png" alt="local-icon">
              <span><?= the_field('local'); ?></span>
            </div>

            <a href="<?= the_permalink(); ?>" class="btn-cta">Saiba mais ➜</a>

          </div>
        </div>


      <?php endwhile;
      wp_reset_postdata(); ?>

      <div class="barradenavegacao ">
        <?php


        echo paginate_links(array(
          'format' =>
          '?pagina=%#%', 'show_all' => false, 'current' => max(1, $paged), 'total' => $eventosRealizados->max_num_pages, 'prev_text' => '<i class="fas fa-caret-left fa-2x"></i>', 'next_text' => '<i class="fas fa-caret-right fa-2x"></i>',
          'type' => 'list'
        ));
        ?>
      </div>

    </div>
  </div>

</section>


<?= get_footer(); ?>