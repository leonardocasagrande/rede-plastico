<section id="faca-parte" class="contato-home">


  <div class="container">
    <?php if(is_page('faca-parte')) : echo 
    '<h2 class="title">Entre em contato</h2>' ; else : echo
    '<h2 class="title">Faça parte</h2>' ; endif; ?>

    <div class="line-title "></div>
  </div>

  <div class="template-contato container">

    <?= do_shortcode('[contact-form-7 id="62" title="Formulário de contato"]'); ?>

  </div>


</section>