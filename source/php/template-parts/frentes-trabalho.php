<div class="list-frentes">
    <div class="list-frentes-desktop d-lg-block d-none">
        <div class="accordion" id="accordionExampleDesktop">
            <div class="container">
                <div class="custom-row">
                    <?php
                    $argsFrentes = array(
                        'post_type' => 'frente-trabalho',
                        'posts_per_page' => -1,
                        'order' => 'ASC'
                    );
                    $frentesTrabalhoDesktop = new WP_Query($argsFrentes);
                    if ($frentesTrabalhoDesktop->have_posts()) : while ($frentesTrabalhoDesktop->have_posts()) : $frentesTrabalhoDesktop->the_post();
                    ?>
                            <div class="frente-item collapsed" id="headingDesktop-<?= $post->post_name; ?> " data-toggle="collapse" data-target="#collapseDesktop-<?= $post->post_name; ?>" aria-expanded="true" aria-controls="collapseDesktop-<?= $post->post_name; ?>">
                                <div class="bg-cinza ">
                                    <?php the_post_thumbnail() ?>
                                    <hr class="linha">
                                    <h2><?= get_the_title() ?></h2>
                                </div>
                            </div>
                    <?php endwhile;
                    endif;
                    wp_reset_postdata(); ?>
                </div>
            </div>
            <?php if (is_page('frentes-de-trabalho')) : ?>
                <div class="container">
                    <?php
                    if ($frentesTrabalhoDesktop->have_posts()) : while ($frentesTrabalhoDesktop->have_posts()) : $frentesTrabalhoDesktop->the_post();
                    ?>


                            <div id="collapseDesktop-<?= $post->post_name; ?>" class="collapse " aria-labelledby="headingDesktop-<?= $post->post_name; ?>" data-parent="#accordionExampleDesktop">
                                <div class="frentes-conteudo">
                                    <div class="row">
                                        <div class="col-4">
                                            <h2 class="title-frentes"><?= get_the_title(); ?></h2>

                                            <div class="foto-do-lider">
                                                <img src="<?= get_field('foto_do_lider') ?>" alt="">
                                            </div>

                                            <span>Líder: <?= get_field('nome_do_lider') ?></span>
                                        </div>
                                        <div class="col-6 direita">
                                            <h3 class="title-frentes">O que buscamos?</h3>
                                            <div class="texto-buscamos">
                                                <?php the_field('o_que_buscamos') ?>
                                            </div>
                                            <div class="empresa">
                                                Responsável:<br><b> <?php the_field('empresa_responsavel') ?> </b>
                                            </div>
                                            <!-- <a href="<?= get_site_url(); ?>/projetos" class="cta-projetos">Veja nossos Projetos</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <?php endwhile;
                    endif;
                    wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="list-frentes-mobile d-lg-none">
        <div class="accordion" id="accordionExampleDesktop">
            <div class="caroussel">
                <div class="carousselFrentes">
                    <?php
                    $argsFrentesMobile = array(
                        'post_type' => 'frente-trabalho',
                        'posts_per_page' => -1,
                        'order' => 'ASC'
                    );
                    $frentesTrabalhoMobile = new WP_Query($argsFrentesMobile);
                    if ($frentesTrabalhoMobile->have_posts()) : while ($frentesTrabalhoMobile->have_posts()) : $frentesTrabalhoMobile->the_post();
                    ?>
                            <div class="item-carosel">
                                <div class="wrap-mobile">
                                    <div class="frente-item collapsed" id="headingMobile-<?= $post->post_name; ?> " data-toggle="collapse" data-target="#collapseMobile-<?= $post->post_name; ?>" aria-expanded="true" aria-controls="collapseMobile-<?= $post->post_name; ?>">
                                        <div class="bg-cinza ">
                                            <?php the_post_thumbnail() ?>
                                            <hr class="linha">
                                            <h2><?= get_the_title() ?></h2>
                                        </div>
                                    </div>
                                    <?php if (is_page('frentes-de-trabalho')) : ?>
                                        <div id="collapseMobile-<?= $post->post_name; ?>" class="collapse " aria-labelledby="headingMobile-<?= $post->post_name; ?>" data-parent="#accordionExampleDesktop">
                                            <div class="frentes-conteudo text-center">

                                                <div class="foto-do-lider row align-items-center justify-content-center">

                                                    <div class=" col-4">
                                                        <img src="<?= get_field('foto_do_lider') ?>" alt="">
                                                    </div>

                                                    <div class="col-7"><span class="text-left">Líder: <b><?= get_field('nome_do_lider') ?> </b></span></div>
                                                </div>
                                                <div class=" direita">
                                                    <h3 class="title-frentes">O que buscamos?</h3>
                                                    <br>
                                                    <div class="texto-buscamos mb-3">
                                                        <?php the_field('o_que_buscamos') ?>
                                                    </div>
                                                    <div class="empresa">
                                                        Responsável:<br><b> <?php the_field('empresa_responsavel') ?> </b>
                                                    </div>
                                                    <!-- <a href="<?= get_site_url(); ?>/projetos" class="cta-projetos">Veja nossos Projetos</a> -->
                                                </div>

                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                    <?php endwhile;
                    endif;
                    wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
</div>