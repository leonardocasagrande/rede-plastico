<?php get_header(); ?>

<section class="retorna d-lg-flex">
    <div class="container">
        <?= get_template_part('breadcrumbs'); ?>

        <h2 class="title pb-lg-0">Conheça de forma gratuita o índice de reciclabilidade da sua embalagem.</h2>

        <div class="d-lg-flex align-items-center justify-content-between">

            <a href="https://retorna.redeplastico.com.br/" target="_bank" class="btn-cta ml-lg-0">Descubra já! ➜</a>

            <img class="mb-5 mb-lg-0" src="<?= the_field('logo_retorna'); ?>" alt="">

        </div>

    </div>

    <img class="col-lg-6 px-0 br-57" src="<?= the_field('campanha_retorna'); ?>" alt="">
</section>


<section class="como-funciona">
    <div class="container d-lg-flex">


        <div class="col-lg-6">
            <h3 class="title">Como funciona a RETORNA e o índice de reciclabilidade?</h3>
            <div class="line-title"></div>

            <p>A RETORNA é a primeira ferramenta online e gratuita que calcula o índice de reciclabilidade da embalagem plástica e que tem como base o cenário brasileiro além das especificidades de cada região.
            </p>

            <p><b>Em um questionário técnico de 3 etapas, avaliamos a sua embalagem usando notas que vão de A até E.</b></p>

            <a href="https://retorna.redeplastico.com.br/" target="_bank" class="btn-cta">Avalie agora ➜</a>

            <span class="mini d-none d-lg-block">*A Rede pela Circularidade do Plástico não é responsável pela validação dos dados imputados na ferramenta, por tanto o índice de reciclabilidade não é considerado uma certificação.</span>

        </div>

        <img class="embalagem" src="<?= the_field('imagem_embalagem'); ?>" alt="">

        <span class="mini  d-lg-none">*A Rede pela Circularidade do Plástico não é responsável pela validação dos dados imputados na ferramenta, por tanto o índice de reciclabilidade não é considerado uma certificação.</span>

    </div>

</section>

<section class="green-section">

    <div class="wrapper-lg-retorna">
        <div class="garrafa-wrapper  d-lg-block d-none">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/garrafa-interativa.png" alt="" class="garrafa">

            <ul class="nav  " id="pills-tab" role="tablist">
                <li class="btn btn1">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fas fa-times"></i></a>
                </li>
                <li class="btn btn2">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fas fa-plus"></i></a>
                </li>
                <li class="btn btn3">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><i class="fas fa-plus"></i></a>
                </li>
                <li class="btn btn4">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-test" role="tab" aria-controls="pills-test" aria-selected="false"><i class="fas fa-plus"></i></a>
                </li>
            </ul>
        </div>

        <div class="col-lg-6 ">
            <div class="container">
                <h2 class="title">Visão técnica e de mercado para análises completas de reciclabilidade no Brasil.</h2>
                <div class="line-title"></div>

                <p class="mb-5">Cada aspecto do seu projeto é avaliado para trazer o melhor resultado e insights técnicos que mudarão o destino do plástico na cadeia.</p>
            </div>

            <div class="garrafa-wrapper d-lg-none">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/garrafa-interativa.png" alt="" class="garrafa">

                <ul class="nav  " id="pills-tab" role="tablist">
                    <li class="btn btn1">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><i class="fas fa-times"></i></a>
                    </li>
                    <li class="btn btn2">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="fas fa-plus"></i></a>
                    </li>
                    <li class="btn btn3">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><i class="fas fa-plus"></i></a>
                    </li>
                    <li class="btn btn4">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-test" role="tab" aria-controls="pills-test" aria-selected="false"><i class="fas fa-plus"></i></a>
                    </li>
                </ul>
            </div>

            <div class="tab-content container" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <h3 class="titulo">Reciclabilidade na prática por região do país - único no Brasil.
                    </h3>
                    <p>Além de determinar se a embalagem pode ser reciclada, a ferramenta também provém a capacidade real de reciclagem por região do país, dando uma visão mais completa não só de quão reciclável ela é, mas também de quão aceita ela é no mercado. O índice é feito com base nas pesquisas nacionais da infraestrutura disponível de cada estado.</p>
                    <!-- <a href="#" class="btn-cta">Calcule o índice ➜</a> -->

                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <h3 class="titulo">Avaliação completa de reciclabilidade do desenho da embalagem.
                    </h3>
                    <p> A ferramenta avalia todos os componentes da embalagem, desde o material até o tipo de decoração utilizado para trazer o quão reciclável é cada material.</p>
                    <!-- <a href="#" class="btn-cta">Calcule o índice ➜</a> -->

                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 class="titulo">Ferramenta interactiva real time</h3>
                    <p> Avalia o impacto na reciclabilidade a nível técnico por escolhas no desenho dos materiais e componentes da embalagem</p>
                    <!-- <a href="#" class="btn-cta">Calcule o índice ➜</a> -->

                </div>
                <div class="tab-pane fade" id="pills-test" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <h3 class="titulo">Diferenciação por embalagens plásticas rígidas e flexíveis.</h3>
                    <p> Você poderá testar o índice de reciclabilidade de diferentes materiais, podemos ser rígidos ou flexíveis de acordo com a necessidade da sua embalagem.</p>
                    <!-- <a href="#" class="btn-cta">Calcule o índice ➜</a> -->

                </div>
            </div>

            <a href="https://retorna.redeplastico.com.br" target="_blank" class="btn-cta ml-4 mt-5">Avalie o índice ➜</a>

        </div>

    </div>

</section>

<section class="duvidas">

    <div class="container">

        <p>Em caso de dúvidas e sugestões sobre a ferramenta, <b>entre em contato com a REDE</b> no email </p>
        <a href="mailto:retorna@redeplastico.org.br">retorna@redeplastico.org.br</a>
    </div>

</section>

<?php get_footer(); ?>