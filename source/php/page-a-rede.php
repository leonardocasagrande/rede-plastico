<?= get_header(); ?>


<section class="sobre-rede">


  <div class="container header-text d-lg-flex justify-content-between">

    <div class=" col-lg-7 mb-lg-4 pr-lg-5">
      <!-- TEMPLATE BREADCRUMBS -->
      <?= get_template_part('breadcrumbs'); ?>

      <h2 class="title"><?= the_field('titulo_sobre_nos'); ?></h2>
      <div class="line-title"></div>

      <p><?= the_field('texto_sobre_nos'); ?></p>

    </div>

    <img class="col-lg-5 d-none d-lg-block px-0" src="<?= the_field('imagem_sobre_nos'); ?>" alt="">

  </div>

  <img class="d-lg-none  px-0" src="<?= the_field('imagem_sobre_nos'); ?>" alt="">


  <!-- <div class="container fazemos-somos-realizamos">


    <div class="carousel-fsr d-lg-flex">


      <?php
      if (have_rows('fazemos')) : the_row();
      ?>

        <div class="item col-lg-4 ">

          <img src="<?= the_sub_field('imagem'); ?>" alt="">

          <div class="wrapper">

            <h3 class="name">Fazemos</h3>
            <div class="line-title"></div>

            <p><?= the_sub_field('texto'); ?></p>

          </div>

        </div>

      <?php
      endif;
      ?>

      <?php
      if (have_rows('somos')) : the_row();
      ?>

        <div class="item col-lg-4 ">

          <img src="<?= the_sub_field('imagem'); ?>" alt="">

          <div class="wrapper">

            <h3 class="name">Somos</h3>
            <div class="line-title"></div>

            <p><?= the_sub_field('texto'); ?></p>

          </div>

        </div>

      <?php
      endif;
      ?>

      <?php
      if (have_rows('realizamos')) : the_row();
      ?>

        <div class="item col-lg-4">

          <img src="<?= the_sub_field('imagem'); ?>" alt="">

          <div class="wrapper">

            <h3 class="name">Realizamos</h3>
            <div class="line-title"></div>

            <p><?= the_sub_field('texto'); ?></p>

          </div>

        </div>

      <?php
      endif;
      ?>


    </div>

  </div> -->

</section>

<section class="conquistas-da-rede-sobre">

  <div class="container header">
    <h2 class="title pt-5">REDE em números</h2>
    <div class="line-title"></div>
  </div>


  <div class="lg-wrapper">

    <div class="col-12 col-lg-6 video px-0 ">
      <?= the_field('video_conquistas_da_rede'); ?>
    </div>

    <div class="wrapper-conquistas-sobre container">


      <div class="carousel-conquistas-sobre">

        <div class="item">

          <div class="wrapper">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/predio-verde.png" alt="prédio">

            <div>

              <h4 class="big"><?= the_field('numero_de_empresas_que_a_rede_tem_elo'); ?></h4>
              <h4 class="small s1">empresas unidas pelo mesmo propósito</h4>

            </div>

          </div>

        </div>

        <div class="item">

          <div class="wrapper">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/maos-verde.png" alt="mãos">

            <div>

              <h4 class="big"><?= the_field('numero_de_elos_com_a_cadeia_de_plastico'); ?> elos</h4>
              <h4 class="small s2">da cadeia de plástico envolvidos</h4>

            </div>

          </div>

        </div>

        <div class="item">

          <div class="wrapper">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pessoas-verde.png" alt="pessoas">

            <div>

              <h4 class="big"><?= the_field('numero_de_voluntarios'); ?></h4>
              <h4 class="small s3">voluntários a favor da Circularidade do Plástico</h4>

            </div>

          </div>

        </div>


      </div>

      <a href="<?= get_site_url(); ?>/nossos-elos" class="btn-cta d-lg-none">Conheça os elos </br>que fazem parte</a>

    </div>

  </div>

  <a href="<?= get_site_url(); ?>/nossos-elos" class="btn-cta d-none d-lg-flex my-lg-5">Conheça os elos que fazem parte</a>


</section>


<section class="circularidade">

  <div class="lg-wrapper">

    <div class="container d-lg-block">
      <h2 class="title">Circularidade</h2>
      <div class="line-title"></div>
    </div>

    <div class="caroussel">

      <div class="container">
        <h3 class="name">Cases</h3>
        <p>Conheça iniciativas de implementação da Economia Circular.</p>
      </div>


      <?php
      $argsArquivos = array(
        'post_type' => 'cases',
        'order' => 'ASC',


      );
      $arquivos = new WP_Query($argsArquivos);


      if ($arquivos->have_posts()) :; ?>

        <div class="carousel-pdf">




          <?php while ($arquivos->have_posts()) :  $arquivos->the_post(); ?>



            <a href="<?= the_permalink(); ?>" class="item">
              <div class="wrapper">
                <div class="icon-box">
                  <div class="fundo-bg" style="background: url(<?= the_post_thumbnail_url(); ?>); background-size: cover; height: 100%; width: 100%; border-radius: 60px;"></div>
                </div>

                <div class="text-eye">
                  <h4 class="titulo">Case <?= the_title(); ?></h4>
                  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eye-icon.png" alt="">
                </div>
              </div>
            </a>



          <?php endwhile; ?>

        </div>

      <?php else : ?>

        <p>Não encontramos nenhum arquivo.</p>

      <?php endif; ?>


    </div>

    <div class="caroussel">

      <div class="container">
        <h3 class="name">Eventos</h3>
        <p>Confira abaixo nosso álbum de eventos.</p>
      </div>



      <?php
      $argsEventos = array(
        'post_type' => 'evento',
        'meta_key'    => 'data',
        'orderby'    => 'meta_value_num',
        'order'      => 'DESC',
      );
      remove_all_filters('posts_orderby');

      $eventos = new WP_Query($argsEventos);

      if ($eventos->have_posts()) :; ?>

        <div class="carousel-eventos">


          <?php while ($eventos->have_posts()) :  $eventos->the_post(); ?>



            <a href="<?= the_permalink(); ?>" class="item">


              <?php $images = get_field('fotos_eventos'); ?>

              <div class="wrapper">
                <img class="thumb" src="<?= $images[0]; ?> " alt="">

                <div class="text-eye">
                  <h4 class="titulo"><?= the_title(); ?></h4>
                  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eye-icon.png" alt="">
                </div>
              </div>

            </a>



          <?php endwhile; ?>

        </div>

      <?php else : ?>

        <p>Não encontramos nenhum evento.</p>

      <?php endif; ?>


    </div>




    <!-- <div class="caroussel">

      <div class="container">
        <h3 class="name">Vídeos</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
      </div>



      <?php
      $argsVideos = array(
        'post_type' => 'video',
        'order' => 'ASC',


      );
      $videos = new WP_Query($argsVideos);
      $conta_video = 0;


      if ($videos->have_posts()) :; ?>

        <div class="wrapper-videos">
          <div class="carousel-videos">


            <?php while ($videos->have_posts()) :  $videos->the_post();

              //second false skip ACF pre-processcing
              $url = get_field('url_video', false, false);
              //get wp_oEmed object, not a public method. new WP_oEmbed() would also be possible
              $oembed = _wp_oembed_get_object();
              //get provider
              $provider = $oembed->get_provider($url);
              //fetch oembed data as an object
              $oembed_data = $oembed->fetch($provider, $url);
              $thumbnail = $oembed_data->thumbnail_url;
              $iframe = $oembed_data->html;


            ?>

              <button type="button" class=" item" data-toggle="modal" data-target="#exampleModal-<?= $conta_video; ?>">
                <div class="wrapper">
                  <img class="thumb" src="<?= $thumbnail; ?>" alt="">

                  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/play-icon.png" alt="" class="play-icon">

                  <div class="text-eye">
                    <h4 class="titulo"><?= the_title(); ?></h4>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/eye-icon.png" alt="">
                  </div>
                </div>
                </a>
              </button>



            <?php $conta_video++;
            endwhile; ?>

          </div>

        <?php else : ?>

          <p>Não encontramos nenhum vídeo.</p>

        <?php endif; ?>


        </div>

    </div> -->
  </div>



</section>


<?php
$argsVideos = array(
  'post_type' => 'video',
  'order' => 'ASC',


);
$videos = new WP_Query($argsVideos);
$conta_video = 0;


if ($videos->have_posts()) :; ?>

  <!-- <div class="wrapper-videos">
    <div class="carousel-videos"> -->


  <?php while ($videos->have_posts()) :  $videos->the_post(); ?>

    <!-- 
        <div class="modal fade" id="exampleModal-<?= $conta_video; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?= the_title(); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <?= the_field('url_video'); ?>
              </div>

            </div>
          </div>
        </div> -->

<?php $conta_video++;
  endwhile;
endif; ?>


<?= get_footer(); ?>