<?php get_header(); ?>


<section class="porque-fazer-parte">
  <div class="container d-lg-flex  justify-content-between">

    <div class="col-lg-5 px-0">
      <?= get_template_part('breadcrumbs'); ?>
      <h2 class="title pr-lg-5"><?= the_field('titulo'); ?></h2>

      <div class="line-title"></div>

      <p><?= the_field('texto'); ?></p>
    </div>

    <div class="col-lg-6 px-0">

      <div class="box-blue">

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-participar-1.png" alt="">

          <h4 class="text">Maior fórum de circularidade do plástico no Brasil.</h4>

        </div>

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-participar-2.png" alt="">

          <h4 class="text">Encontro entre os elos da cadeia, facilitando a comunicação.</h4>

        </div>

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-participar-3.png" alt="">

          <h4 class="text">Ser parte de mudanças para o futuro de todos.</h4>

        </div>


      </div>

      

    </div>

  </div>


  <div class=" custom-container">

    <img class="col-lg-5 px-0" src="<?= the_field('imagem_conteudo');?>" alt="">

    <div class="container content-wrapper">
      <p><?= the_content();?></p>

      <!-- <a href="#faca-parte" class="btn-cta">Faça parte da REDE</a> -->
    </div>
  </div>

</section>





<!-- <section class="passos">


  <div class="passos-box">
    <?php

    $passo = 1;
    // Check value exists.
    if (have_rows('passo')) :

      // Loop through rows.
      while (have_rows('passo')) : the_row(); ?>



        <div class="passo-item d-lg-flex">
          <img class="col-lg-7 px-0" src="<?= the_sub_field('imagem'); ?>" alt="">

          <div class="container ">
            <h2 class="title"><span><?= $passo; ?>º passo</span>.</h2>

            <p><?= the_sub_field('texto'); ?></p>
          </div>
        </div>

    <?php $passo++;
      endwhile;



    endif; ?>
  </div>

  <div class="green-box ">

    <div class="container ">
      <div class="col-lg-8 px-0 d-lg-flex align-items-center">
        <h3>Pessoas físicas</h3>

        <span class="d-none d-lg-block mx-md-2"></span>

        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
      </div>

      <a href="#" class="btn-cta">Entrar em contato</a>
    </div>

  </div>
</section> -->

<?= get_template_part('contact-form'); ?>


<?php get_footer(); ?>