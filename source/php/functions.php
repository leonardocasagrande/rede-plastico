<?php


function my_function_admin_bar()
{
	return false;
}
add_filter('show_admin_bar', 'my_function_admin_bar');

add_theme_support('post-thumbnails');





add_action('rest_api_init', 'add_thumbnail_to_JSON');
function add_thumbnail_to_JSON()
{
	//Add featured image
	register_rest_field(
		array(
			'produtos',
			'post', 'receita',
		), // Where to add the field (Here, blog posts. Could be an array)
		'featured_image_src', // Name of new field (You can call this anything)
		array(
			'get_callback'    => 'get_image_src',
			'update_callback' => null,
			'schema'          => null,
		)
	);
}

function get_image_src($object, $field_name, $request)
{
	$feat_img_array = wp_get_attachment_image_src(
		$object['featured_media'], // Image attachment ID
		'full',  // Size.  Ex. "thumbnail", "large", "full", etc..
		true // Whether the image should be treated as an icon.
	);
	return $feat_img_array[0];
}





//Auto add and update Title field:
function my_post_title_updater($post_id)
{

	$my_post = array();
	$my_post['ID'] = $post_id;

	// custom post type
	$depoimentos    = get_field('depoimentos');

	if (get_post_type() == 'depoimento') {
		$my_post['post_title'] = get_field('nome_do_autor');
	}

	// Update the post into the database
	wp_update_post($my_post);
}

// run after ACF saves the $_POST['fields'] data
add_action('acf/save_post', 'my_post_title_updater', 20);
//END Auto add and update Title field:
function cptui_register_my_cpts()
{

	/**
	 * Post Type: Depoimentos.
	 */

	$labels = [
		"name" => __("Depoimentos", "custom-post-type-ui"),
		"singular_name" => __("Depoimento", "custom-post-type-ui"),
	];

	$args = [
		"label" => __("Depoimentos", "custom-post-type-ui"),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => ["slug" => "depoimento", "with_front" => true],
		"query_var" => true,
		"supports" => false,
		"show_in_graphql" => false,
	];

	register_post_type("depoimento", $args);



	/**
	 * Post Type: Projetos.
	 */

	$labels = [
		"name" => __("Projetos", "custom-post-type-ui"),
		"singular_name" => __("Projeto", "custom-post-type-ui"),
	];

	$args = [
		"label" => __("Projetos", "custom-post-type-ui"),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => ["slug" => "projeto", "with_front" => true],
		"query_var" => true,
		"supports" => ["title", "editor", "thumbnail", "custom-fields"],
		"show_in_graphql" => false,
	];

	register_post_type("projeto", $args);

	/**
	 * Post Type: Vídeos.
	 */

	$labels = [
		"name" => __("Vídeos", "custom-post-type-ui"),
		"singular_name" => __("Vídeo", "custom-post-type-ui"),
	];

	$args = [
		"label" => __("Vídeos", "custom-post-type-ui"),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => ["slug" => "video", "with_front" => true],
		"query_var" => true,
		"supports" => ["title", "thumbnail"],
		"show_in_graphql" => false,
	];

	register_post_type("video", $args);

	/**
	 * Post Type: Eventos.
	 */

	$labels = [
		"name" => __("Eventos", "custom-post-type-ui"),
		"singular_name" => __("Evento", "custom-post-type-ui"),
	];

	$args = [
		"label" => __("Eventos", "custom-post-type-ui"),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => ["slug" => "evento", "with_front" => true],
		"query_var" => true,
		"supports" => ["title", "editor", "thumbnail"],
		"show_in_graphql" => false,
	];

	register_post_type("evento", $args);
}

add_action('init', 'cptui_register_my_cpts');

function cptui_register_my_cpts_nossos_elos()
{

	/**
	 * Post Type: Nossos Elos.
	 */

	$labels = [
		"name" => __("Nossos Elos", "custom-post-type-ui"),
		"singular_name" => __("Elo", "custom-post-type-ui"),
	];

	$args = [
		"label" => __("Nossos Elos", "custom-post-type-ui"),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => ["slug" => "nossos-elos", "with_front" => true],
		"query_var" => true,
		"supports" => ["title", "editor", "thumbnail", "page-attributes"],
		"show_in_graphql" => false,
	];

	register_post_type("nossos-elos", $args);
}

add_action('init', 'cptui_register_my_cpts_nossos_elos');

function cptui_register_my_cpts_frente_trabalho()
{

	/**
	 * Post Type: Frentes de trabalho.
	 */

	$labels = [
		"name" => __("Frentes de trabalho", "custom-post-type-ui"),
		"singular_name" => __("Frente de trabalho", "custom-post-type-ui"),
	];

	$args = [
		"label" => __("Frentes de trabalho", "custom-post-type-ui"),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => ["slug" => "frente-trabalho", "with_front" => true],
		"query_var" => true,
		"supports" => ["title", "editor", "thumbnail"],
		"show_in_graphql" => false,
	];

	register_post_type("frente-trabalho", $args);
}

add_action('init', 'cptui_register_my_cpts_frente_trabalho');



function cptui_register_my_taxes()
{

	/**
	 * Taxonomy: Situação.
	 */

	$labels = [
		"name" => __("Situação", "custom-post-type-ui"),
		"singular_name" => __("Situação", "custom-post-type-ui"),
	];


	$args = [
		"label" => __("Situação", "custom-post-type-ui"),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => ['slug' => 'situacao', 'with_front' => true,],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "situacao",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy("situacao", ["evento"], $args);
}
add_action('init', 'cptui_register_my_taxes');


function cptui_register_my_cpts_cases()
{

	/**
	 * Post Type: Cases.
	 */

	$labels = [
		"name" => __("Cases", "custom-post-type-ui"),
		"singular_name" => __("Case", "custom-post-type-ui"),
	];

	$args = [
		"label" => __("Cases", "custom-post-type-ui"),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => ["slug" => "cases", "with_front" => true],
		"query_var" => true,
		"supports" => ["title", "editor", "thumbnail"],
		"show_in_graphql" => false,
	];

	register_post_type("cases", $args);
}

add_action('init', 'cptui_register_my_cpts_cases');
?>