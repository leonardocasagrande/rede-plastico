<?php get_header();
$pagedProximos = (isset($_GET['pagina_proximos']) ? $_GET['pagina_proximos'] : 1);
$pagedAnteriores = (isset($_GET['pagina_anteriores']) ? $_GET['pagina_anteriores'] : 1);
?>

<section class="eventos">

  <div class="d-lg-flex ">
    <div class="container col-lg-6">

      <?= get_template_part('breadcrumbs'); ?>

      <h3 class="title">Eventos</h3>

      <div class="line-title"></div>

      <p><?= the_content(); ?></p>

    </div>

    <img class="pt-5 pt-lg-0 col-lg-6 px-0 aside-img" src="<?= the_post_thumbnail_url(); ?>" alt="">
  </div>

  <div class="container eventos-estrutura pt-5">

    <?php
    wp_reset_postdata();
    $argsEventos =   array(
      'post_type' => 'evento',
      'posts_per_page' => '4',
      'paged' => $pagedProximos,
      'meta_key'    => 'data',
      'orderby'    => 'meta_value_num',
      'order'      => 'DESC',

      'tax_query' => array(
        array(
          'taxonomy' => 'situacao',
          'field' => 'slug',
          'terms' => 'proximos-eventos',
        )

      )
    );

    remove_all_filters('posts_orderby');

    $eventos = new WP_Query($argsEventos);

    if ($eventos->have_posts()) :


    ?>

      <h4 class="sub">Próximos eventos</h4>

      <div class="wrapper">



        <?php


        while ($eventos->have_posts()) : $eventos->the_post();


        ?>

          <div class="item d-lg-flex col-lg-6">
            <div class="bg-item" style="background: url(<?= the_post_thumbnail_url(); ?>); background-position: center;"></div>
            <div class="infos col-lg-7">

              <h5 class="titulo"><?= the_title(); ?></h5>
              <div class="line-title"></div>

              <!-- <p><?= the_content(); ?></p> -->

              <div class="data-local pt-3">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/data.png" alt="data-icon">
                <span><?= the_field('data'); ?></span>
              </div>

              <div class="data-local">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/local.png" alt="local-icon">
                <span><?= the_field('local'); ?></span>
              </div>

              <a href="<?= the_permalink(); ?>" class="btn-cta">Saiba mais ➜</a>

            </div>
          </div>

      <?php endwhile;
      endif;
      wp_reset_postdata(); ?>

      <div class="barradenavegacao ">
        <?php


        echo paginate_links(array(
          'format' =>
          '?pagina_proximos=%#%', 'show_all' => false, 'current' => max(1, $pagedProximos), 'total' => $eventos->max_num_pages, 'prev_text' => '<i class="fas fa-caret-left fa-2x"></i>', 'next_text' => '<i class="fas fa-caret-right fa-2x"></i>',
          'type' => 'list'
        ));
        ?>
      </div>


      </div>

  </div>


  <div class="container eventos-estrutura anteriores pt-5">

    <h4 class="sub">Eventos anteriores</h4>
    <div class="wrapper">

      <?php
      wp_reset_postdata();
      $argsEventos =   array(
        'post_type' => 'evento',
        'meta_key'    => 'data',
        'orderby'    => 'meta_value_num',
        'order'      => 'DESC',
        'posts_per_page' => '4',
        'paged' => $pagedAnteriores,


        'tax_query' => array(
          array(
            'taxonomy' => 'situacao',
            'field' => 'slug',
            'terms' => 'eventos-realizados',
          )

        )
      );
      remove_all_filters('posts_orderby');
      $eventosRealizados = new WP_Query($argsEventos);


      while ($eventosRealizados->have_posts()) : $eventosRealizados->the_post();


      ?>


        <div class="item d-lg-flex col-lg-6">
          <div class="bg-item" style="background: url(<?= the_post_thumbnail_url(); ?>); background-position: center;"></div>
          <div class="infos col-lg-7">

            <h5 class="titulo"><?= the_title(); ?></h5>
            <div class="line-title"></div>

            <!-- <p><?= the_content(); ?></p> -->

            <div class="wrapper-dl">
              <div class="data-local pt-3">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/data.png" alt="data-icon">
                <span><?= the_field('data'); ?></span>
              </div>

              <div class="data-local">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/local.png" alt="local-icon">
                <span><?= the_field('local'); ?></span>
              </div>
            </div>

            <a href="<?= the_permalink(); ?>" class="btn-cta">Saiba mais ➜</a>

          </div>
        </div>


      <?php endwhile;
      wp_reset_postdata(); ?>

      <div class="barradenavegacao ">
        <?php


        echo paginate_links(array(
          'format' =>
          '?pagina_anteriores=%#%', 'show_all' => false, 'current' => max(1, $pagedAnteriores), 'total' => $eventosRealizados->max_num_pages, 'prev_text' => '<i class="fas fa-caret-left fa-2x"></i>', 'next_text' => '<i class="fas fa-caret-right fa-2x"></i>',
          'type' => 'list'
        ));
        ?>
      </div>

    </div>
  </div>

</section>

<?php get_footer(); ?>