<?php get_header(); ?>
<div class="page-frentes">
<div class="sobre-rede">
    <div class="container header-text d-lg-flex justify-content-between">

<div class=" col-lg-7 mb-lg-4 pr-lg-5">
  <!-- TEMPLATE BREADCRUMBS -->
  <?= get_template_part('breadcrumbs'); ?>

  <h2 class="title"><?= the_field('titulo_frentes'); ?></h2>
  <div class="line-title"></div>

  <p><?= the_field('texto_frentes'); ?></p>

    </div>

    <img class="col-lg-5 d-none d-lg-block px-0" src="<?= the_field('imagem_frentes'); ?>" alt="">

    </div>

    <img class="d-lg-none  px-0" src="<?= the_field('imagem_frentes'); ?>" alt="">
    </div>
    </div>
<div class="mb-4">
<?php get_template_part('frentes-trabalho') ?>
</div>
<?php get_footer(); ?>