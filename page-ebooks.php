<?php get_header(); ?> <section class="eventos"><div class="d-lg-flex"><div class="container col-lg-6"> <?= get_template_part('breadcrumbs'); ?> <h3 class="title">E-books</h3><div class="line-title"></div><p><?= the_content(); ?></p></div><img class="pt-5 pt-lg-0 col-lg-6 px-0 aside-img" src="<?= the_post_thumbnail_url(); ?>" alt=""></div><div class="container eventos-estrutura pt-5"> <?php
    wp_reset_postdata();
    $argsebooks =   array(
      'post_type' => 'ebook',
      'posts_per_page' => '-1'
      
    );


    $ebooks = new WP_Query($argsebooks);

    if ($ebooks->have_posts()) :


    ?> <h4 class="sub">Conheça nossos E-books</h4><div class="wrapper row"> <?php


        while ($ebooks->have_posts()) : $ebooks->the_post();


        ?> <div class="item d-lg-flex col-lg-6"><div class="bg-item d-lg-none" style="background: url(<?= the_field('imagem_destaque_mobile'); ?>); background-position: center;"></div><div class="bg-item d-none d-lg-block" style="background: url(<?= the_field('imagem_destaque_desktop'); ?>); background-position: center;"></div><div class="infos col-lg-7"><h5 class="titulo"><?= the_title(); ?></h5><div class="line-title"></div><a href="#exampleModal-<?= $post->post_name ?>" data-toggle="modal" data-target="#exampleModal-<?= $post->post_name ?>" class="btn-cta">Baixar e-book <i class="fa-solid fa-arrow-down-to-line"></i></a></div></div> <?php endwhile;
      ?> <!-- <div class="col-12"><a href="" class="btn-cta my-4">Ver Mais</a></div> --> <?php
      endif;
      wp_reset_postdata(); ?> </div></div></section> <?php
    wp_reset_postdata();
    $argsebooks =   array(
      'post_type' => 'ebook',
      'posts_per_page' => '-1'
      
    );


    $ebooks = new WP_Query($argsebooks);

    if ($ebooks->have_posts()) :


    ?> <?php


while ($ebooks->have_posts()) : $ebooks->the_post();


?> <!-- Modal --><div class="modal fade" id="exampleModal-<?= $post->post_name ?>" tabindex="-1" aria-labelledby="exampleModal-<?= $post->post_name ?>Label" aria-hidden="true"><div class="modal-dialog"><div class="modal-content contato-home p-0"><div class="modal-header"><h1 class="modal-title fs-5 text-center" id="exampleModal-<?= $post->post_name ?>Label">Baixar <?= the_title() ?></h1><button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button></div><div class="modal-body d-flex align-items-center"><div class="w-100"><div class="template-contato"> <?= do_shortcode('[contact-form-7 id="891" title="Formulário PDF_copy"]') ?> </div></div></div></div></div></div> <?php endwhile; endif;
      wp_reset_postdata(); ?> <?php get_footer(); ?> <script> <?php
    wp_reset_postdata();
    $argsebooks =   array(
      'post_type' => 'ebook',
      'posts_per_page' => '-1'
      
    );


    $ebooks = new WP_Query($argsebooks);

    if ($ebooks->have_posts()) :


    ?> <?php

$contador = 1;
while ($ebooks->have_posts()) : $ebooks->the_post();


?> var wpcf7Elm<?= $post->ID ?> = document.querySelector( '#wpcf7-f891-o<?= $contador ?>' );
  
  wpcf7Elm<?= $post->ID ?>.addEventListener( 'wpcf7submit', function( event ) {
    var link = document.createElement('a');
    link.href = '<?= get_field('arquivo_do_ebook') ?>';
    link.download = 'E-book <?= the_title() ?>';
    link.dispatchEvent(new MouseEvent('click'));
  }, false ); <?php $contador++; endwhile; endif;
      wp_reset_postdata(); ?> </script>