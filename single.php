<?

the_post();



?> <?= get_header(); ?> <?php $post_id = $post->ID;;  ?> <section class="single-blog"><img class="mx-auto d-block" src="<?= the_post_thumbnail_url(); ?>" alt=""><div class="container"> <?= get_template_part('breadcrumbs'); ?> <h3 class="title"><?= the_title(); ?></h3></div><div class="container conteudo"><p><?= the_content(); ?></p></div><div class="next-post container"><div class="post d-lg-none"><h3 class="title">Artigos relacionados</h3><div class="line-title"></div><p class="pre-post">Confira outros artigos escritos por nós.</p> <?php

      wp_reset_postdata();
      $argsPost =   array(
        'post_type' => 'post',
        'posts_per_page' => '1',
        'orderby' => 'rand',
      );

      $posts = new WP_Query($argsPost);


      while ($posts->have_posts()) : $posts->the_post();
      ?> <img src="<?= the_post_thumbnail_url(); ?>" alt=""><h4 class="titulo"><?= the_title(); ?></h4><p><?= the_excerpt(); ?></p><a href="<?= the_permalink(); ?>" class="btn-cta">Ler artigo ➜</a> <?php endwhile; ?> </div><div class="d-none d-lg-block wrapper-posts"> <?php

      wp_reset_postdata();
      $argsPost =   array(
        'post_type' => 'post',
        'posts_per_page' => 6,
        'orderby' => 'rand',
        'post__not_in' => array($post_id),
        'category__not_in' => array( 1,7,8 )


      );

      $posts = new WP_Query($argsPost);


      if ($posts->have_posts()) :;
      ?> <h3 class="title">Artigos relacionados</h3><div class="line-title"></div><p class="pre-post">Confira outros artigos escritos por nós.</p><div class="carousel-posts-lg"> <?php while ($posts->have_posts()) : $posts->the_post();
          ?> <div class="item"><div class="img" style="background: url(<?= the_post_thumbnail_url(); ?>) center;"></div><h4 class="titulo"><?= the_title(); ?></h4><p><?= the_excerpt(); ?></p><a href="<?= the_permalink(); ?> " class="btn-cta">Ler artigo ➜</a></div> <?php endwhile; ?> </div> <?php endif; ?> </div></div></section> <?= get_footer(); ?>