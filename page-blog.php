<?php get_header(); ?> <section class="eventos"><div class="d-lg-flex"><div class="container col-lg-6"> <?= get_template_part('breadcrumbs'); ?> <h3 class="title">Blog</h3><div class="line-title"></div><p><?= the_content(); ?></p></div><img class="pt-5 pt-lg-0 col-lg-6 px-0 aside-img" src="<?= the_post_thumbnail_url(); ?>" alt=""></div><div class="container eventos-estrutura pt-5"><h4 class="sub">Artigos - Blog</h4><div class="wrapper"> <?php
      wp_reset_postdata();
      $argsPosts =   array(
        'post_type' => 'post',
        'posts_per_page' => '4',
        'category__not_in' => array( 8,7,1 )
        
      );
      $PostsRealizados = new WP_Query($argsPosts);


      while ($PostsRealizados->have_posts()) : $PostsRealizados->the_post();


      ?> <div class="item d-lg-flex col-lg-6"><div class="bg-item d-lg-none" style="background: url(<?= the_field('imagem_destaque_mobile'); ?>); background-position: center;"></div><div class="bg-item d-none d-lg-block" style="background: url(<?= the_field('imagem_destaque_desktop'); ?>); background-position: center;"></div><div class="infos col-lg-7"><h5 class="titulo"><?= the_title(); ?></h5><div class="line-title"></div><p><?= the_excerpt(); ?></p><div class="wrapper-dl"><div class="data-local pt-3"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/data.png" alt="data-icon"> <span><?= the_date(); ?></span></div></div><a href="<?= the_permalink(); ?>" class="btn-cta">Ler mais ➜</a></div></div> <?php endwhile;
      wp_reset_postdata(); ?> <!-- <div class="col-12"><a href="" class="btn-cta my-4">Ver Mais</a></div> --></div></div></section> <?php get_footer(); ?>