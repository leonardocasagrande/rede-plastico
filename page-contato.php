<?= get_header(); ?> <section class="contato"><!-- <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/fundo-contato-lg.png" alt="" class="fundo-contato"> --><div class="wrapper-contato"><div class="container col-lg-6 pl-lg-0 pr-lg-5 m-lg-0"> <?= get_template_part('breadcrumbs'); ?> <h2 class="title"><?= the_title(); ?></h2><div class="line-title"></div><p><?= the_content(); ?></p><a href="<?= get_site_url(); ?>/faca-parte" class="btn-cta">Minha empresa quer fazer parte</a></div><div class="gray-box col-lg-6"><div class="container"><div class="item"><h3 class="titulo"><i class="fas fa-envelope mr-3"></i>E-mail</h3><p class="custom-p-lg">Fale conosco:</p><a href="mailto:<?= the_field('email_fale_conosco'); ?>"><?= the_field('email_fale_conosco'); ?></a><!-- <p class="custom-p-lg">Administrativo:</p>
          <a href="mailto:<?= the_field('email_administrativo'); ?>"><?= the_field('email_administrativo'); ?></a>

          <p class="custom-p-lg">Imprensa:</p>
          <a href="mailto:<?= the_field('email_imprensa'); ?>"><?= the_field('email_imprensa'); ?></a> --></div><!-- <div class="item telefones">
          <h3 class="titulo"><i class="fas fa-phone-alt mr-3"></i>Telefone</h3>

          <div>
            <p>Atendimento: </p><a href="#">(19) 9999.6666</a>
          </div>
          <div>
            <p>Whatsapp: </p><a href="#">(19) 9999.6666 <i class="fab fa-whatsapp ml-2"></i></a>
          </div>

        </div> --><div class="item"><h3 class="titulo"><i class="fas fa-phone-alt mr-3"></i> Redes sociais</h3><div class="midias"><span class="d-inline pr-2">@circulaplastico</span> <a target="_blank" href="https://www.instagram.com/circulaplastico/"><i class="fab fa-instagram"></i></a> <a target="_blank" href="https://www.linkedin.com/company/circulaplastico/"><i class="fab fa-linkedin"></i></a> <a target="_blank" href="https://www.facebook.com/circulaplastico"><i class="fab fa-facebook-square"></i></a></div></div></div></div></div></section> <?= get_footer(); ?>