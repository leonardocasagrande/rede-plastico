<?php if (is_page(array('home', 'faca-parte'))) : $d_none = ' d-none';
endif; ?> <section class="pre-footer <?= $d_none; ?>"><div class="container"><!-- <div class="d-lg-flex align-items-center"> --><h4 class="big">Vamos Circular essa ideia?</h4><!-- <span class="d-none d-lg-inline mx-md-3"></span> --><!-- <p>Pela Economia Circular</p> --><!-- </div> --><div class="btn-wrapper"><a href="<?= get_site_url(); ?>/faca-parte" class="btn-cta mt-4 mt-lg-0">Junte-se a nós!</a> <a href="#" class="up-btn d-lg-block d-none"><i class="fas fa-arrow-up"></i></a></div></div></section><footer><div class="container position-relative"><a class="col-lg-2 col-md-3 col-7 d-block mb-lg-0 px-0" href="<?= get_site_url(); ?>/"><img class="logo-white" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-white.png" alt=""></a><div class="links d-lg-flex col-lg-10 px-lg-0"><!-- <a href="<?= get_site_url(); ?>/o-plastico">O Plástico</a> --> <a href="<?= get_site_url(); ?>/a-rede">A REDE</a> <a href="<?= get_site_url(); ?>/nossos-elos">Nossos Elos </a><a href="<?= get_site_url(); ?>/frentes-de-trabalho">Frentes de Trabalho </a><!-- <a href="<?= get_site_url(); ?>/retorna">Retorna </a> --> <a href="<?= get_site_url(); ?>/blog">Blog </a><a href="<?= get_site_url(); ?>/eventos">Eventos </a><a href="<?= get_site_url(); ?>/contato">Contato</a><!-- <a href="<?= get_site_url(); ?>/retorna"><img style="width: 150px;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/retorna-logo.png" class="mb-lg-0" alt=""></a> --> <a class="btn-faca" href="<?= get_site_url(); ?>/faca-parte">Faça Parte</a><div class="midias-header px-0 col-6 col-lg-1 px-lg-0"><a href="https://www.instagram.com/circulaplastico/" target="_blank"><i class="fab fa-instagram"></i></a> <a href="https://www.linkedin.com/company/circulaplastico/" target="_blank"><i class="fab fa-linkedin"></i></a> <a href="https://www.facebook.com/circulaplastico" target="_blank"><i class="fab fa-facebook-f"></i></a></div><!-- <a class="btn-restrict" href="http://redeplastico.com.br/site/rede-indice.asp?secao=8"><i class="fas fa-lock pr-2"></i> Área Restrita</a> --></div></div><div class="gray-area"><div class="cont-lg"><span class="mobilizacao d-lg-none">Mobilização: <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mob-logo.png" alt=""></span><span>© 2021 - Rede pela Circularidade do Plástico</span> <span class="mobilizacao d-none d-lg-flex">Mobilização: <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mob-logo.png" alt=""></span><span>Desenvolvido por <a href="https://www.humann.com.br/" target="_blank"><b>Humann</b></a></span></div></div></footer><script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script><script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js" integrity="sha512-k2GFCTbp9rQU412BStrcD/rlwv1PYec9SNrkbQlo6RZCf75l6KcC3UwDY8H5n5hl4v77IDtIPwOk9Dqjs/mMBQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.3/jquery.inputmask.min.js"></script><script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script> <?php if (is_page('nossos-elos') || is_front_page()) : ?> <script>$(function() { <?php
      $argsElosMobile = array(
        'post_type' => 'nossos-elos',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'post__not_in' => array(382)
      );
      $elosMobile = new WP_Query($argsElosMobile);
      if ($elosMobile->have_posts()) : while ($elosMobile->have_posts()) : $elosMobile->the_post(); ?> var sliderClientesMobile = tns({
            container: ".carossel-clientes-mobile-<?php echo $post->post_name; ?>",
            items: 1,
            slideBy: 1,
            autoplayButtonOutput: false,
            nav: true,
            navPosition: 'bottom',
            controls: false,
            loop: false,
          }); <?php endwhile;
      endif;
      wp_reset_postdata(); ?> <?php
      $argsElosMobile = array(
        'post_type' => 'nossos-elos',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'post__not_in' => array(382)
      );
      $elosMobile = new WP_Query($argsElosMobile);
      if ($elosMobile->have_posts()) : while ($elosMobile->have_posts()) : $elosMobile->the_post(); ?> var sliderClientesDesktop = tns({
            container: ".carossel-clientes-desktop-<?php echo $post->post_name; ?>",
            items: 1,
            slideBy: 1,
            autoplayButtonOutput: false,
            nav: true,
            navPosition: 'bottom',
            controls: false,
            loop: false,
          }); <?php endwhile;
      endif;
      wp_reset_postdata(); ?> });</script> <?php endif; ?> <?php if (is_page('frentes-de-trabalho') || is_front_page()) : ?> <script>$(function() {
      var sliderFrentesMobile = tns({
        container: ".carousselFrentes",
        items: 1.2,
        slideBy: 1,
        gutter: 15,
        edgePadding: 30,
        autoplayButtonOutput: false,
        nav: false,
        controls: false,
        loop: false,
      });

    });</script> <?php endif; ?> <?php if (is_page('home')) : ?> <script>$(function() {
      var sliderBannerHome = tns({
        container: ".banner-home",
        items: 1,
        slideBy: 1,
        autoplayButtonOutput: false,
        controls: false,
        nav: true,
        navPosition: 'bottom',
        loop: true,
        autoplayTimeout: 3000,
        autoplay: true,
      });
    });

    $(function() {
      var sliderBannerHome = tns({
        container: ".banner-home-mob",
        items: 1,
        slideBy: 1,
        autoplayButtonOutput: false,
        controls: false,
        nav: true,
        navPosition: 'bottom',
        autoplayTimeout: 3000,
        loop: true,
        autoplay: true,

      });
    });

    $(function() {
      var sliderInfosPlastico = tns({
        container: ".carousel-infos",
        items: 1,
        slideBy: 1,
        autoplayButtonOutput: false,
        controls: false,
        nav: true,
        navPosition: 'bottom',
        loop: false,
        responsive: {
          1000: {
            items: 2
          },
          1200: {
            disable: true,

          }
        }
      });
    });


    $(function() {
      var sliderConquistas = tns({
        container: ".carousel-conquistas",
        items: 1,
        slideBy: 1,
        autoplayButtonOutput: false,
        controls: false,
        nav: true,
        navPosition: 'bottom',
        navContainer: '.conquistas-ctn',
        loop: false,
        responsive: {

          700: {
            disable: true,
          }
        }
      });
    });

    $(function() {
      var sliderDepoimentos = tns({
        container: ".carousel-depoimentos",
        items: 1,
        slideBy: 1,
        autoplayButtonOutput: false,
        nav: false,
        controlsText: [
          "<i class='fas fa-arrow-left'></i>",
          "<i class='fas fa-arrow-right'></i>",
        ],
        controlsPosition: 'bottom',
        loop: false,
      });
    });</script> <?php endif; ?> <?php if (is_page('a-rede')) : ?> <script>$(function() {
      var sliderFSR = tns({
        container: ".carousel-fsr",
        items: 1,
        slideBy: 1,
        autoplayButtonOutput: false,
        nav: true,
        navPosition: 'bottom',
        controls: false,
        loop: false,
        responsive: {
          1000: {
            disable: true,
          }
        }
      });
    });



    $(function() {
      var sliderConquistasSobre = tns({
        container: ".carousel-conquistas-sobre",
        items: 1,
        slideBy: 1,
        autoplayButtonOutput: false,
        nav: true,
        navPosition: 'bottom',
        controls: false,
        loop: false,
        responsive: {
          1000: {
            disable: true,
          }
        }
      });
    });

    $(function() {
      var sliderPdf = tns({
        container: ".carousel-pdf",
        items: 1.2,
        slideBy: 1,
        gutter: 15,
        edgePadding: 30,
        autoplayButtonOutput: false,
        nav: false,
        controls: false,
        loop: false,
        responsive: {
          800: {
            items: 2.3,
            controls: true,
            gutter: 15,
            edgePadding: 0,
            controlsText: [
              "<i class='fas fa-arrow-left'></i>",
              "<i class='fas fa-arrow-right'></i>",
            ],
          },
          1000: {
            items: 3,

          },

          1200: {
            items: 5,
          }
        }
      });
    });

    $(function() {
      var sliderEventos = tns({
        container: ".carousel-eventos",
        items: 1.2,
        slideBy: 1,
        gutter: 15,
        edgePadding: 30,
        autoplayButtonOutput: false,
        nav: false,
        controls: false,
        loop: false,
        responsive: {
          800: {
            items: 2.3,
            controls: true,
            gutter: 15,
            edgePadding: 0,
            controlsText: [
              "<i class='fas fa-arrow-left'></i>",
              "<i class='fas fa-arrow-right'></i>",
            ],
          },
          1000: {
            items: 3,

          },

          1200: {
            items: 5,
          }
        }
      });
    });

    $(function() {
      var sliderVideos = tns({
        container: ".carousel-videos",
        items: 1.2,
        slideBy: 1,
        gutter: 15,
        edgePadding: 30,
        autoplayButtonOutput: false,
        nav: false,
        controls: false,
        loop: false,
        responsive: {
          800: {
            items: 2.3,
            controls: true,
            gutter: 15,
            edgePadding: 0,
            controlsText: [
              "<i class='fas fa-arrow-left'></i>",
              "<i class='fas fa-arrow-right'></i>",
            ],
          },
          1000: {
            items: 3,

          },

          1200: {
            items: 5,
          }
        }
      });
    });</script> <?php endif; ?> <?php if (is_single() && 'post') : ?> <script>$(function() {
      var sliderBlog = tns({
        container: ".carousel-posts-lg",
        items: 3,
        slideBy: 1,
        autoplayButtonOutput: false,
        nav: false,
        navPosition: "bottom",
        controls: true,
        loop: false,
        gutter: 100,
        controlsText: [
          "<i class='fas fa-arrow-left'></i>",
          "<i class='fas fa-arrow-right'></i>",
        ],
      });
    });</script> <?php endif; ?> <?php if (is_single() && 'evento') : ?> <script>$(function() {
      var sliderGaleria = tns({
        container: ".carousel-galeria",
        items: 5,
        slideBy: 1,
        autoplayButtonOutput: false,
        nav: false,
        navPosition: "bottom",
        controls: true,
        loop: false,
        gutter: 15,
        controlsText: [
          "<i class='fas fa-arrow-left'></i>",
          "<i class='fas fa-arrow-right'></i>",
        ],
      });
    });

    $(function() {
      var sliderFotosEventos = tns({
        container: ".carousel-fotos",
        items: 1,
        slideBy: 1,
        autoplayButtonOutput: false,
        nav: false,
        navPosition: "bottom",
        controls: false,
        loop: false,
      });
    });</script> <?php endif; ?> <script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script> <?php wp_footer(); ?>